﻿using Newtonsoft.Json;

namespace QTwitter.Entities
{
    public class QTwitterUser
    {
        public QTwitterUser()
        {
            
        }

        [JsonProperty(PropertyName = "id")]
        public decimal Id { get; set; }

        [JsonProperty(PropertyName = "screen_name")]
        public string ScreenName { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "profile_image_url_https")]
        public string ProfileImageLocation { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Website { get; set; }
    }
}