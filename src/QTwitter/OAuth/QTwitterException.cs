﻿using System;
using System.Net;

namespace QTwitter.OAuth
{
    public class QTwitterException : Exception
    {
        public QTwitterException(string message, WebException wex):base(message, wex)
        {
            
        }
    }
}