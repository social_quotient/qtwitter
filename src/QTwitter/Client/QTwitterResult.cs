﻿using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace QTwitter
{
    public class QTwitterResult<TEntity>
    {
        internal QTwitterResult(HttpWebResponse response)
        {
            ParseStatus(response);
            ParseRateLimit(response);
            if (this.Status != ResponseStatus.Success)
                return;

            ParseResult(response);
        }

        private void ParseRateLimit(HttpWebResponse response)
        {
            string limitHeader = response.Headers["X-Rate-Limit-Limit"];
            string remainingHeader = response.Headers["X-Rate-Limit-Remaining"];
            if (string.IsNullOrEmpty(limitHeader))
                return;

            int limit;
            int remaining;
            int.TryParse(limitHeader, out limit);
            int.TryParse(remainingHeader, out remaining);
        }

        private void ParseResult(HttpWebResponse response)
        {
            var stream = response.GetResponseStream();

            using (var reader = new StreamReader(stream))
            {
                this.Result = JsonConvert.DeserializeObject<TEntity>(reader.ReadToEnd());
            }
        }

        private void ParseStatus(HttpWebResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    this.Status = ResponseStatus.Success;
                    break;
                case HttpStatusCode.Unauthorized:
                    this.Status = ResponseStatus.Unauthorized;
                    break;
                case HttpStatusCode.BadRequest:
                    this.Status = ResponseStatus.BadRequest;
                    break;
                case HttpStatusCode.RequestTimeout:
                    this.Status = ResponseStatus.ConnectionTimedOut;
                    break;
                case HttpStatusCode.NotFound:
                    this.Status = ResponseStatus.NotFound;
                    break;
                default:
                    this.Status = ResponseStatus.Unknown;
                    break;
            }
        }

        public ResponseStatus Status { get; private set; }

        public TEntity Result { get; private set; }

        public TwitterRateLimit RateLimit { get; set; }
    }
}