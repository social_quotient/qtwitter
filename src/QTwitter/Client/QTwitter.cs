﻿//-----------------------------------------------------------------------
// <copyright file="TwitterConsumer.cs" company="Outercurve Foundation">
//     Copyright (c) Outercurve Foundation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;
using System.Text;
using QTwitter.Entities;
using QTwitter.OAuth;

namespace QTwitter
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net;
    using System.Web;

    [Serializable]
    public class QTwitter
    {
        private OAuthTokens _tokens;

        private const string ApiEndpoint = "https://api.twitter.com/1.1";


        public QTwitter(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
        {
            _tokens = new OAuthTokens
                {
                    ConsumerKey = consumerKey,
                    ConsumerSecret = consumerSecret,
                    AccessToken = accessToken,
                    AccessTokenSecret = accessTokenSecret
                };
        }

        public QTwitterResult<QTwitterUser> VerifyCredentials()
        {
            return GetApiResponse<QTwitterUser>(HTTPVerb.GET, "/account/verify_credentials.json");
        }

        public QTwitterResult<QTweet> Update(string message)
        {
            return GetApiResponse<QTweet>(HTTPVerb.POST, "/statuses/update.json", new Dictionary<string,string>{ {"status", message} });
        }

        private static string ApiUrl(string relativeUrl)
        {
            return string.Format("{0}{1}", ApiEndpoint, relativeUrl);
        }

        private QTwitterResult<TResponse> GetApiResponse<TResponse>(HTTPVerb verb, string url, Dictionary<string, string> parameters = null)
        {
            QTwitterResult<TResponse> result = null;
            var req = new WebRequestBuilder(new Uri(ApiUrl(url)), verb, _tokens);
            if (parameters != null)
            {
                foreach (var key in parameters.Keys)
                {
                    req.Parameters.Add(key, parameters[key]);
                }
            }
            try
            {
                var res = req.ExecuteRequest();
                result = new QTwitterResult<TResponse>(res);
            }
            catch (WebException ex)
            {
                string message = "Api Error";
                if (ex.Response != null)
                {
                    using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        message = reader.ReadToEnd();
                    }
                }
                throw new QTwitterException(message, ex);
            }
            return result;
        }
    }
}
