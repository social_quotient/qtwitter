﻿namespace QTwitter
{
    public enum ResponseStatus
    {
        BadRequest,
        ConnectionTimedOut,
        NotFound,
        NotAcceptable,
        ProxyAuthenticationRequired,
        RateLimited,
        Success,
        TwitterIsDown,
        TwitterIsOverloaded,
        Unauthorized,
        Unknown,
    }
}