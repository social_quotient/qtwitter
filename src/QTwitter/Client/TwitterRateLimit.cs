﻿namespace QTwitter
{
    public class TwitterRateLimit
    {
        public int Limit { get; set; }
        public int Remaining { get; set; }
    }
}